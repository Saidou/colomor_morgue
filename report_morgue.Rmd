---
output: 
  pdf_document: 
    number_sections: yes
papersize: a4
geometry: "left=2.0cm, right=1.0cm, top=2.5cm, bottom=2.0cm"
header-includes:
  - \usepackage{booktabs}
  - \usepackage{float}
  - \usepackage{tabu}
  - \renewcommand{\familydefault}{\sfdefault}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-13, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\vspace{0.5cm}

\begin{center}
\Large{\textbf{Estimating COVID-19 Excess Mortality from local civil registries and facilities in Gabon (COMOLOR)}}\\
Data from MORGUE
\end{center}

\vspace{0.5cm}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(dplyr)
library(knitr)
library(kableExtra)

```


```{r table1}
tab_1 %>% 
  kable(booktabs = T, format = "latex", caption = "Distribution of deaths according to age and gender",
      linesep = " ", col.names = c(" ", "2012", "2013", "2014", "2019", "2020", "2021"), 
      align = c("l", "r", "r", "r", "r", "r", "r"),
      format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(row = 0, bold = TRUE) %>%
  pack_rows(" ", 1, 1) %>%
  pack_rows("Age group", 2, 4) %>%
  pack_rows("Gender", 5, 7)
```


```{r table2}
tab_2 %>%
  kable(caption = "Monthly deaths per year", format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "2012", "2013", "2014", "2019", "2020", "2021"),
        format.args = list(big.mark = ' '), align = c("l", "r", "r", "r", "r", "r", "r")) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(c(0, nrow(tab_2)), bold = T) %>%
  column_spec(1, bold = T) %>%
  pack_rows(" ", nrow(tab_2), nrow(tab_2))
```


```{r fig.cap="Estimates of excess deaths in 2020 data from MORGUE", fig.pos='h', fig.height=3.5, fig.width=6}
fig_1
```


